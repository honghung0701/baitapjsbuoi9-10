var staffList = [];
var dataJson =localStorage.getItem("DSNV");
if (dataJson) {
    var dataRaw = JSON.parse(dataJson);
    var result = [];
    for (var index = 0; index <dataRaw.length; index ++) {
        var currentData = dataRaw[index];
        var nv = new staff (
            currentData.account,
            currentData.name,
            currentData.email,
            currentData.pass,
            currentData.workDay,
            currentData.salary,
            currentData.position,
            currentData.workTime
        );
        result.push(nv);
    };
    staffList = result;
    renderStaffList(staffList);
}
function saveLocalStorage() {
    var dsnvJson = JSON.stringify(staffList);
    localStorage.setItem("DSNV",dsnvJson);
}
document.getElementById('btnThemNV').onclick = function() {
    var newNv = getInformationForm();
    var isValid = true;
    isValid &= checkEmpty(newNv.account,"tbTKNV") && checkAccount(newNv.account,"tbTKNV");
    isValid &= checkEmpty(newNv.name,"tbTen") && checkName(newNv.name,"tbTen");
    isValid &= checkEmpty(newNv.email,"tbEmail") && checkEmail(newNv.email,"tbEmail");
    isValid &= checkEmpty(newNv.pass,"tbMatKhau") && checkPass(newNv.pass,"tbMatKhau");
    isValid &= checkEmpty(newNv.workDay,"tbNgay") && checkDay(newNv.workDay,"tbNgay");
    isValid &= checkEmpty(newNv.salary,"tbLuongCB") && checkSalary(newNv.salary,"tbLuongCB");
    isValid &= checkPosition(newNv.position,"tbChucVu");
    isValid &= checkEmpty(newNv.workTime,"tbGiolam") && checkWorkTime(newNv.workTime,"tbGiolam");
    if (isValid) {
        staffList.push(newNv);
        saveLocalStorage();
        renderStaffList(staffList);
        resetForm();
    }
}
function delStaff(idNv) {
    var index = staffList.findIndex(function(nv) {
        return nv.account == idNv;
    });
    if (index == -1) {
        return;
    }
    staffList.splice(index,1);
    saveLocalStorage();
    renderStaffList(staffList);
}
function fixStaff(idNv) {
    var index = staffList.findIndex(function (nv) {
        return nv.account == idNv;
    });
    if (index == -1) {
        return;
    };
    var nv = staffList[index];
    showInformationForm(nv);
}
document.getElementById("btnCapNhat").onclick = function () {
    var nvEdit = getInformationForm();
    var isValid = true;
    isValid &= checkEmpty(nvEdit.account,"tbTKNV") && checkAccount(nvEdit.account,"tbTKNV");
    isValid &= checkEmpty(nvEdit.name,"tbTen") && checkName(nvEdit.name,"tbTen");
    isValid &= checkEmpty(nvEdit.email,"tbEmail") && checkEmail(nvEdit.email,"tbEmail");
    isValid &= checkEmpty(nvEdit.pass,"tbMatKhau") && checkPass(nvEdit.pass,"tbMatKhau");
    isValid &= checkEmpty(nvEdit.workDay,"tbNgay") && checkDay(nvEdit.workDay,"tbNgay");
    isValid &= checkEmpty(nvEdit.salary,"tbLuongCB") && checkSalary(nvEdit.salary,"tbLuongCB");
    isValid &= checkPosition(nvEdit.position,"tbChucVu");
    isValid &= checkEmpty(nvEdit.workTime,"tbGiolam") && checkWorkTime(nvEdit.workTime,"tbGiolam");
    if (isValid) {
        var index = staffList.findIndex(function (nv) {
        return nv.account == nvEdit.account;
        });
        if (index == -1) return;
        staffList[index] = nvEdit;
        saveLocalStorage();
        renderStaffList(staffList);
        resetForm();
    }
}
function searchRank() {
    var valueSearch = document.getElementById("searchName").value;
    var rankSearch = staffList.filter(value => {
        return value.rank().toUpperCase().includes(valueSearch.toUpperCase());
    });
    console.log(rankSearch);
    renderStaffList(rankSearch);
}