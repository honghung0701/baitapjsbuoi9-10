function getInformationForm() {
    var account = document.getElementById('tknv').value;
    var name = document.getElementById('name').value;
    var email = document.getElementById('email').value;
    var pass = document.getElementById('password').value;
    var workDay = document.getElementById('datepicker').value;
    var salary = document.getElementById('luongCB').value;
    var position = document.getElementById('chucvu').value;
    var workTime = document.getElementById('gioLam').value;
    var staffs = new staff(account,name,email,pass,workDay,salary,position,workTime);
    return staffs;
}
function renderStaffList(list) {
    var contentHTML = "";
    for (var i = 0; i < list.length; i++) {
        var currentStaffs = list[i];
        var contentTr = `<tr>
        <td>${currentStaffs.account}</td>
        <td>${currentStaffs.name}</td>
        <td>${currentStaffs.email}</td>
        <td>${currentStaffs.workDay}</td>
        <td>${currentStaffs.position}</td>
        <td>${currentStaffs.totalSalary()}</td>
        <td>${currentStaffs.rank()}</td>
        <td>
        <button onclick = "delStaff('${currentStaffs.account}')" class="btn btn-danger">Xóa</button>
        <button data-toggle="modal" data-target="#myModal" class="btn btn-primary" onclick = "fixStaff('${currentStaffs.account}')">Sửa</button>
        </td>
        </tr>`
        contentHTML += contentTr
    }
    document.getElementById('tableDanhSach').innerHTML = contentHTML;
}
function showInformationForm(nhanVien) {
    document.getElementById('tknv').value = nhanVien.account;
    document.getElementById('name').value = nhanVien.name;
    document.getElementById('email').value = nhanVien.email;
    document.getElementById('password').value = nhanVien.pass;
    document.getElementById('datepicker').value = nhanVien.workDay;
    document.getElementById('luongCB').value = nhanVien.salary; 
    document.getElementById('chucvu').value = nhanVien.position;
    document.getElementById('gioLam').value = nhanVien.workTime;
}
function resetForm() {
    document.getElementById("formQLNV").reset();
}