function checkEmpty(value,idError) {
    if (value.length == 0) {
        document.getElementById(idError).style.display = "block";
        document.getElementById(idError).innerText = "Trường này không được để trống";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function checkAccount(value,idError) {
    if (value.length >= 4 && value.length <= 6) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).style.display = "block";
        document.getElementById(idError).innerText = "Tài khoản phải có tối đa 4-6 ký số";
        return false;
    }
}
function checkName(value,idError) {
    var letters = /^[A-Za-z]+$/;
    if (value.match(letters)) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).style.display = "block";
        document.getElementById(idError).innerText = "Tên nhân viên phải là chữ";
        return false;
    }
}
function checkEmail(value,idError) {
    const re =   
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail = re.test(value);
    if (!isEmail) {
        document.getElementById(idError).style.display = "block";
        document.getElementById(idError).innerText = "Email không hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;    
    }
}
function checkPass(value,idError) {
    var upperCaseLetters = /[A-Z]/g;
    var numbers = /[0-9]/g;
    var specialCharacter = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    if (value.match(upperCaseLetters) && value.match(numbers) && value.match(specialCharacter)) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).style.display = "block";
        document.getElementById(idError).innerText = "Phải chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký đặc biệt";
        return false;
    }
    
}
function checkDay(value,idError) {
  const regex = /^\d{2}\/\d{2}\/\d{4}$/;

  if (value.match(regex) === null) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "Ngày làm phải có định dạng mm/dd/yy";
    return false;
  }

  const [month, day, year] = value.split('/');

  // 👇️ format Date string as `yyyy-mm-dd`
  const isoFormattedStr = `${year}-${month}-${day}`;

  const date = new Date(isoFormattedStr);

  const timestamp = date.getTime();

  if (typeof timestamp !== 'number' || Number.isNaN(timestamp)) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "Ngày làm phải có định dạng mm/dd/yy";
    return false;
  }
  document.getElementById(idError).innerText = "";
  return date.toISOString().startsWith(isoFormattedStr);
}
function checkSalary(value,idError) {
    if (value >= 1000000 && value <= 20000000) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).style.display = "block";
        document.getElementById(idError).innerText = "Lương cơ bản 1000000 - 20000000";
        return false;
    }
}
function checkPosition(value,idError) {
    if (value == "") {
        document.getElementById(idError).style.display = "block";
        document.getElementById(idError).innerText = "Phải chọn chức vụ hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText ="";
        return true
    }
}
function checkWorkTime (value,idError) {
    if (value >= 80 && value <= 200) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).style.display = "block";
        document.getElementById(idError).innerText = "Số giờ làm 80 - 200 giờ";
        return false;
    }
}