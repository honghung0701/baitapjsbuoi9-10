function staff (_account,_name,_email,_pass,_workDay,_salary,_position,_workTime) {
    this.account = _account;
    this.name = _name;
    this.email = _email;
    this.pass = _pass;
    this.workDay = _workDay;
    this.salary = _salary;
    this.position = _position;
    this.workTime = _workTime;
    this.totalSalary = function() {
        if (this.position == "Sếp") {
            return this.salary*3;
        } else if (this.position == "Trưởng phòng") {
            return this.salary*2;
        } else {
            return this.salary;
        }
    }
    this.rank = function() {
        if (this.workTime*1 >= 192) {
            return "Nhân viên xuất sắc";
        } else if (this.workTime*1 >= 176) {
            return "Nhân viên giỏi";
        } else if (this.workTime*1 >= 160) {
            return "Nhân viên khá";
        } else {
            return "Nhân viên trung bình";
        }
    }
}